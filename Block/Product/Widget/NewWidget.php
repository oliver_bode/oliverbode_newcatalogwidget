<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Oliverbode\NewCatalogWidget\Block\Product\Widget;

/**
 * Catalog Products List widget block
 * Class ProductsList
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class NewWidget extends \Magento\Catalog\Block\Product\Widget\NewWidget
{
    public function getSliderOptions()
    {   
        return $this->getOptions();
    }
}


