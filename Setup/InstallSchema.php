<?php
namespace Oliverbode\NewCatalogWidget\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Cms\Model\Page;
use Magento\Cms\Controller\Adminhtml\Page\PostDataProcessor;


class InstallSchema implements InstallSchemaInterface {

    protected $pageFactory;

    protected $dataProcessor;

    public function __construct(
    	Page $pageFactory,
    	PostDataProcessor $dataProcessor
    ) 
    {
        $this->_pageFactory = $pageFactory;
        $this->dataProcessor = $dataProcessor;
    }

    private function _toArray($xml) {
        $array = json_decode(json_encode($xml), true);
        foreach (array_slice($array, 0) as $key => $value) {
            if (is_string($value)) $array[$key] = trim($value);
            elseif (is_array($value) && empty($value)) $array[$key] = NULL;
            elseif (is_array($value)) {
                $array[$key] = $this->_toArray($value);
            }
        }
        return $array;
    }

    private function getModuleDir() {
        $manual = BP . '/app/code/Oliverbode/NewCatalogWidget/';
        $composer = BP . '/vendor/oliverbode/newcatalogwidget/';
        if (file_exists($manual)) return $manual;
        else if (file_exists($composer)) return $composer;
        else die(__('Unable to find module directory'));
    }

    private function importPage() {
        $pagesFile = $this->getModuleDir() . 'Setup/data/pages.xml';
        $xml = simplexml_load_string(file_get_contents($pagesFile), null, LIBXML_NOCDATA);
        $cmsPage = $this->_pageFactory;
        try {
            foreach ($xml->pages as $page) {
                $pageId = $cmsPage->getCollection()
                    ->addFieldToFilter('identifier', $page->identifier)
                    ->getFirstItem()
                    ->getId();
                $dataArray = $this->_toArray($page);
                foreach($dataArray as $key => $value) {
                    if ($key == 'store_id') {
                        for ($i = 0; $i < count($dataArray['store_id']['store']); $i++) {
                            $data[$key][$i] = $dataArray['store_id']['store'][$i];
                        }
                    }
                    else $data[$key] = $value;
                }
                $cmsPage->setPageId($pageId);
                $cmsPage->setData($data);
                $cmsPage->save();
            }
        } catch (\Exception $exception) {
            die($exception->getMessage());
        }
    }

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
    	$this->importPage();
    }
}