# Magento2 New Catalog Widget #

Magento2 New Catalog Widget

## Features ##
* Well organized, easy to use and professionally designed.
* Uses the Magento 2 core Cms widget/blocks/pages functionality. 
* Extends the default New Catalog Widget by putting it in an owl slider

## Install Magento 2 New Catalog Widget ##
### Manual Installation ###

Install New Catalog Widget

* Download the extension
* Unzip the file
* Create a folder {Magento root}/app/code/Oliverbode/NewCatalogWidget
* Copy the content from the unzip folder
* Optionally remove Setup folder if you don't want to install the demo

### Composer Installation ###

```
#!

composer config repositories.oliverbode_newcatalogwidget vcs https://oliver_bode@bitbucket.org/oliver_bode/oliverbode_newcatalogwidget.git
composer require oliverbode/newcatalogwidget
```

## Enable New Catalog Widget ##

```
#!

php -f bin/magento module:enable --clear-static-content Oliverbode_NewCatalogWidget
php -f bin/magento setup:upgrade
php -f bin/magento setup:static-content:deploy
```

## Creating a New Catalog Widget Slider ##

Goto Content -> Widget and create a new widget. Choose the "Catalog New Product List"

Select the pages where you wish to insert the widget and the block where you would like it to display. In the "Widget Options" fill in the widget options and save widget.



### Removing New Catalog Widget ###

The demo can be viewed at: {Magento2 Base Url}/http:/new-product-slider/

To remove it: log into your Magetno Admin, then goto Content -> Pages

Delete the "New Catalog Widget".